console.log("inside contentScript");

window.addEventListener("message", function(event) {
  // We only accept messages from ourselves
  if (event.source != window)
    return;

    console.log("message received");

  if (event.data.type && (event.data.type == "EXTENSION_CHECK")) {
    console.log("Content script received: " + event.data.text);
    window.postMessage({ type: "EXTENSION_CHECK_REPLY", text: "true" }, "*");
    // port.postMessage(event.data.text);
  }
}, false);
