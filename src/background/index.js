
console.log("inside background.js");

chrome.runtime.onMessageExternal.addListener(
  (message, sender, sendResponse) => {
    console.log("message request received");
    console.log(message);
    if (message.message == "version") {
      const manifest = chrome.runtime.getManifest();
      sendResponse({
        type: "success",
        version: manifest.version
      });
    }
    return true;
  }
);

chrome.contentSettings["camera"].get(
  {
    primaryUrl: "http://localhost:7000/*"
  },
  function(details) {
    console.log("Camera");
    console.log(details);
    chrome.contentSettings["camera"].set({
      primaryPattern: "http://localhost:7000/*",
      setting: "allow"
    });
  }
);
chrome.contentSettings["microphone"].get(
  {
    primaryUrl: "http://localhost:7000/*"
  },
  function(details) {
    console.log("Microphone");
    console.log(details);
    chrome.contentSettings["microphone"].set({
      primaryPattern: "http://localhost:7000/*",
      setting: "allow"
    });
  }
);
chrome.contentSettings["camera"].get(
  {
    primaryUrl: "https://logicodetest.com/*"
  },
  function(details) {
    console.log("Camera");
    console.log(details);
    chrome.contentSettings["camera"].set({
      primaryPattern: "https://logicodetest.com/*",
      setting: "allow"
    });
  }
);
chrome.contentSettings["microphone"].get(
  {
    primaryUrl: "https://logicodetest.com/*"
  },
  function(details) {
    console.log("Microphone");
    console.log(details);
    chrome.contentSettings["microphone"].set({
      primaryPattern: "https://logicodetest.com/*",
      setting: "allow"
    });
  }
);

chrome.contentSettings["camera"].get(
  {
    primaryUrl: "https://www.codelq.com/*"
  },
  function(details) {
    console.log("Camera");
    console.log(details);
    chrome.contentSettings["camera"].set({
      primaryPattern: "https://www.codelq.com/*",
      setting: "allow"
    });
  }
);
chrome.contentSettings["microphone"].get(
  {
    primaryUrl: "https://www.codelq.com/*"
  },
  function(details) {
    console.log("Microphone");
    console.log(details);
    chrome.contentSettings["microphone"].set({
      primaryPattern: "https://www.codelq.com/*",
      setting: "allow"
    });
  }
);

// console.log("Content script executed");
//         chrome.tabs.executeScript({
//           file: 'contentScript.js'
//         },()=>{
//           console.log("Successfully executed");
//         });


chrome.runtime.onInstalled.addListener(function() {

  chrome.runtime.onMessageExternal.addListener(
    (message, sender, sendResponse) => {
      console.log("message request received");
      console.log(message);
      if (message.message == "version") {
        const manifest = chrome.runtime.getManifest();
        sendResponse({
          type: "success",
          version: manifest.version
        });
      }
      return true;
    }
  );

})
